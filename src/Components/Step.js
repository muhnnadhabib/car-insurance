import React from "react";
import editIcon from "../assest/edit-icon.svg";
const Step = ({ name, selected, i, setCurrentStep, order, objKey }) => {
  function selectStep() {
    setCurrentStep(i + 1);
  }
  return (
    <div className="step" style={{ opacity: selected ? "100%" : "50%" }}>
      <div>{order[objKey] ? <i className="fas fa-check-circle"></i> : ""}</div>
      <p className="step-name">{name}</p>
      <img onClick={selectStep} src={editIcon} alt="edit icon" />
      <p className="step-value">
        {typeof order[objKey] === "string" ? order[objKey] : ""}
      </p>
    </div>
  );
};

export default Step;
