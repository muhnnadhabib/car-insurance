import React from "react";
//import components
import Step from "./Step";
//import imgs
import logo from "../assest/white-logo.png";
import carWhite from "../assest/Car white.png";

const SideSection = ({ currentStep, setCurrentStep, order }) => {
  const steps = [
    { name: "RTO & City", key: "city" },
    { name: "Manufacturer", key: "manufacturer" },
    { name: "Model", key: "model" },
    { name: "Variant", key: "variant" },
    { name: "Registration Year", key: "year" },
    { name: "Previous Policy Expiring Status", key: "prevInsurerAndPolicyExp" },
  ];

  return (
    <div className="side-section">
      <div className="side-intro">
        <img src={logo} alt="logo" />
        <img src={carWhite} alt="white car" />
      </div>

      <div className="steps-container">
        {steps.map((step, i) => {
          return (
            <Step
              key={i}
              i={i}
              name={step.name}
              selected={i + 1 <= currentStep}
              setCurrentStep={setCurrentStep}
              order={order}
              objKey={step.key}
            />
          );
        })}
      </div>
    </div>
  );
};

export default SideSection;
