import React, { useState } from "react";
import { Link } from "react-router-dom";
//Importign the steps
import Step1 from "./Steps/Step1";
import Step2 from "./Steps/Step2";
import Step3 from "./Steps/Step3";
import Step4 from "./Steps/Step4";
import Step5 from "./Steps/Step5";
import Step6 from "./Steps/Step6";

const stepsList = [Step1, Step2, Step3, Step4, Step5, Step6];

const Content = ({
  order,
  setOrder,
  setCurrentStep,
  currentStep,
  setShouldContinue,
  shouldContinue,
}) => {
  const CurrentStepComponent = stepsList[currentStep - 1];
  function nextStep() {
    if (!shouldContinue) return alert("there is an empty field!");
    setCurrentStep((prev) => prev + 1);
    setShouldContinue(false);
  }

  function prevStep() {
    setCurrentStep((prev) => prev - 1);
    setShouldContinue(true);
  }

  return (
    <div
      className="content"
      style={currentStep === 6 ? { backgroundColor: "unset" } : {}}
    >
      <CurrentStepComponent
        order={order}
        setOrder={setOrder}
        setShouldContinue={setShouldContinue}
      />
      <div className="content-footer">
        <a
          style={currentStep > 1 ? null : { display: "none" }}
          href="#"
          onClick={prevStep}
        >
          <i className="fas fa-arrow-left"></i> Previous Step
        </a>
        {currentStep !== 6 ? (
          <button onClick={nextStep}>
            Next Step
            <i className="fas fa-arrow-right"></i>
          </button>
        ) : (
          <Link to="/get-quote">
            <button>
              Get Quote
              <i className="fas fa-arrow-right"></i>
            </button>
          </Link>
        )}
      </div>
    </div>
  );
};

export default Content;
