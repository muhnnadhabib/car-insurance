import React from "react";

const Header = () => {
  return (
    <div className="header">
      <button>
        Products <i className="fas fa-plus"></i>
      </button>
      <div className="profile-bar">
        <img
          src="https://img.freepik.com/free-photo/handsome-young-businessman-shirt-eyeglasses_85574-6228.jpg?size=626&ext=jpg"
          alt="profile image"
        />
        <p>Evan Yates</p>
        <i className="fas fa-chevron-down"></i>
      </div>
    </div>
  );
};

export default Header;
