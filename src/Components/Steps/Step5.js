import React, { useState } from "react";
import sendIcon from "../../assest/send.svg";
const Step5 = ({ order, setOrder, setShouldContinue }) => {
  const [yearList, setyearList] = useState([
    "2021",
    "2020",
    "2019 ",
    "2018",
    "2017",
    "2016",
    "2015",
    "2014",
    "2013",
  ]);
  const [inputText, setInputText] = useState("");
  function selectYear(year) {
    setOrder((prev) => {
      return { ...prev, year };
    });

    setShouldContinue(true);
  }

  function inputHandler(e) {
    const text = e.target.value;

    setInputText(text);
  }

  return (
    <div className="step-component step1">
      <p className="counter">STEP 5/6</p>
      <h2 className="step-title">Vehicle variant</h2>
      <input
        onChange={inputHandler}
        value={inputText}
        type="text"
        placeholder="Search your vehicle variant"
      />

      <ul className="list variant-list">
        {yearList.map((year, i) => {
          return (
            <li
              key={i}
              onClick={() => selectYear(year)}
              className={order.year === year ? "selected-city" : ""}
            >
              {year}
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default Step5;
