import React, { useState } from "react";
import sendIcon from "../../assest/send.svg";
const Step1 = ({ order, setOrder, setShouldContinue }) => {
  const [cityList, setCityList] = useState([
    {
      name: "Ahmedabed",
      items: ["Bengaluru", "skkda", "alaa"],
    },
    {
      name: "Bengaluru",
      items: ["Bengaluru", "skkda", "alaa"],
    },
    {
      name: "Chennai",
      items: ["Bengaluru", "Chennai", "alaa"],
    },
    {
      name: "Delhi",
      items: ["Bengaluru", "skkda", "Chennai"],
    },
    {
      name: "Hyderabad",
      items: ["Chennai", "skkda", "alaa"],
    },
    {
      name: "Jaipur",
      items: ["Bengaluru", "skkda", "Chennai"],
    },
    {
      name: "JAYASHANKAR",
      items: ["Bengaluru", "Chennai", "alaa"],
    },
  ]);
  const [inputText, setInputText] = useState("");
  const [subList, setSubList] = useState(null);
  function selectCity(city) {
    setOrder((prev) => {
      return { ...prev, city };
    });

    setShouldContinue(true);
  }

  function inputHandler(e) {
    const text = e.target.value;

    setInputText(text);
  }

  function clickHandler() {
    if (inputText === "") return;

    setCityList((prev) => [...prev, inputText]);
    setOrder((prev) => {
      return { ...prev, city: inputText };
    });
    setInputText("");
    setShouldContinue(true);
  }
  return (
    <div className="step-component step1">
      <p className="counter">STEP 1/6</p>
      <h2 className="step-title">RTO & City</h2>
      <input
        onChange={inputHandler}
        value={inputText}
        type="text"
        placeholder="Ex: DL-01 Delhi"
      />
      <button onClick={clickHandler}>
        <img src={sendIcon} alt="send icon" />
      </button>

      <ul className="list city-list">
        {cityList.map((city, i) => {
          return (
            <li
              key={i}
              onClick={(e) => {
                if (subList !== null) {
                  subList.children[0].classList.remove("seen-flex");
                  subList.style.marginBottom = "0";
                }
                setSubList(e.target);
                e.target?.children[0]?.classList?.add("seen-flex");
                e.target.style.marginBottom =
                  e.target?.children[0]?.offsetHeight + "px";
              }}
            >
              {city.name}
              <ul>
                {city.items.map((item) => {
                  return (
                    <li
                      onClick={() => {
                        selectCity(item);
                      }}
                    >
                      {item}
                    </li>
                  );
                })}
              </ul>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default Step1;
