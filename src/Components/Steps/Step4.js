import React, { useState } from "react";
import sendIcon from "../../assest/send.svg";
const Step4 = ({ order, setOrder, setShouldContinue }) => {
  const [variantList, setVariantList] = useState([
    "1.3 DX (1343 CC)",
    "1.4 D-4D GD SP*",
    "1.4 D-4D JD ",
    "1.4 D-4D VD SP* ",
    "1.4 D-4D VXD ",
    "1.5 G PETROL",
    "1.5 J PETROL ",
    "1.5 V SP* PETROL ",
    "TRD SPORTIVO DIESEL ",
  ]);
  const [inputText, setInputText] = useState("");
  function selectModel(variant) {
    setOrder((prev) => {
      return { ...prev, variant };
    });

    setShouldContinue(true);
  }

  function inputHandler(e) {
    const text = e.target.value;

    setInputText(text);
  }

  return (
    <div className="step-component step1">
      <p className="counter">STEP 4/6</p>
      <h2 className="step-title">Vehicle variant</h2>
      <input
        onChange={inputHandler}
        value={inputText}
        type="text"
        placeholder="Search your vehicle variant"
      />

      <ul className="list variant-list">
        {variantList.map((variant, i) => {
          return (
            <li
              key={i}
              onClick={() => selectModel(variant)}
              className={order.variant === variant ? "selected-city" : ""}
            >
              {variant}
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default Step4;
