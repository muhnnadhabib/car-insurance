import React, { useState } from "react";
import sendIcon from "../../assest/send.svg";
const Step1 = ({ order, setOrder, setShouldContinue }) => {
  const [modelList, setModelList] = useState([
    "CAMRY",
    "COROLLA",
    "COROLLA ALTIS",
    "ETIOS",
    "Etios Cross",
    "FORTUNER",
    "INNOVA",
    "Innova Crysta",
    "QUALIS",
    "VITZ",
  ]);
  const [inputText, setInputText] = useState("");
  function selectModel(model) {
    setOrder((prev) => {
      return { ...prev, model };
    });

    setShouldContinue(true);
  }

  function inputHandler(e) {
    const text = e.target.value;

    setInputText(text);
  }

  return (
    <div className="step-component step1">
      <p className="counter">STEP 3/6</p>
      <h2 className="step-title">Vehicle Model</h2>
      <input
        onChange={inputHandler}
        value={inputText}
        type="text"
        placeholder="Search your vehicle Model"
      />

      <ul className="list model-list">
        {modelList.map((model, i) => {
          return (
            <li
              key={i}
              onClick={() => selectModel(model)}
              className={order.model === model ? "selected-city" : ""}
            >
              {model}
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default Step1;
