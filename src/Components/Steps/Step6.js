import React, { useEffect, useState } from "react";
import sendIcon from "../../assest/send.svg";
const Step6 = ({ order, setOrder, setShouldContinue }) => {
  useEffect(() => {
    const prevInsurerAndPolicyExp = {
      comprehensivePolicy: true,
      thirdParty: false,
      OD: false,
      rememberPrevPolicy: true,
      prevExp: "Not Expired",
      prevPolicyType: "Comprehensive",
    };

    setOrder((prev) => {
      return { ...prev, prevInsurerAndPolicyExp };
    });

    setShouldContinue(true);
  }, []);

  function onCheck(e) {
    const eleKey = e.target.name;

    setOrder((prev) => {
      return {
        ...prev,
        prevInsurerAndPolicyExp: {
          ...order.prevInsurerAndPolicyExp,
          [eleKey]: !order.prevInsurerAndPolicyExp[eleKey],
        },
      };
    });
  }

  return (
    <div className="step-component step6">
      <p className="counter">STEP 6/6</p>
      <h2 className="step-title">Previous Insurer and Policy Expiry</h2>
      <div className="inputs-container">
        <div className="inputs-box">
          <h4>Required Policy Type</h4>
          <div className="inputs">
            <label>
              <input
                name="comprehensivePolicy"
                onChange={onCheck}
                type="checkbox"
                checked={
                  order?.prevInsurerAndPolicyExp?.comprehensivePolicy
                    ? true
                    : false
                }
              />
              Comprehensive Policy?
            </label>
            <label>
              <input
                name="thirdParty"
                onChange={onCheck}
                type="checkbox"
                checked={
                  order?.prevInsurerAndPolicyExp?.thirdParty ? true : false
                }
              />
              Third Party only?
            </label>
            <label>
              <input
                name="OD"
                onChange={onCheck}
                type="checkbox"
                checked={order?.prevInsurerAndPolicyExp?.OD ? true : false}
              />
              OD Only?
            </label>
          </div>
        </div>
        <div className="inputs-box">
          <h4>Do you remember previous policy details?</h4>
          <div className="inputs">
            <label>
              <input
                type="radio"
                name="policyState"
                onChange={(e) => {
                  setOrder((prev) => {
                    return {
                      ...prev,
                      prevInsurerAndPolicyExp: {
                        ...order.prevInsurerAndPolicyExp,
                        rememberPrevPolicy: true,
                      },
                    };
                  });
                }}
                checked={
                  order?.prevInsurerAndPolicyExp?.rememberPrevPolicy
                    ? true
                    : false
                }
              />
              Yes
            </label>
            <label>
              <input
                type="radio"
                name="policyState"
                checked={
                  !order?.prevInsurerAndPolicyExp?.rememberPrevPolicy
                    ? true
                    : false
                }
                onChange={(e) => {
                  setOrder((prev) => {
                    return {
                      ...prev,
                      prevInsurerAndPolicyExp: {
                        ...order.prevInsurerAndPolicyExp,
                        rememberPrevPolicy: false,
                      },
                    };
                  });
                }}
              />
              No
            </label>
          </div>
        </div>
        <div className="inputs-box">
          <h4>Previous Expiry Status</h4>
          <div className="inputs">
            <label>
              <input
                type="radio"
                name="expiryStatus"
                onChange={() => {
                  setOrder((prev) => {
                    return {
                      ...prev,
                      prevInsurerAndPolicyExp: {
                        ...order.prevInsurerAndPolicyExp,
                        prevExp: "Not Expired",
                      },
                    };
                  });
                }}
                checked={
                  order?.prevInsurerAndPolicyExp?.prevExp === "Not Expired"
                    ? true
                    : false
                }
              />
              Not Expired
            </label>
            <label>
              <input
                type="radio"
                name="expiryStatus"
                onChange={() => {
                  setOrder((prev) => {
                    return {
                      ...prev,
                      prevInsurerAndPolicyExp: {
                        ...order.prevInsurerAndPolicyExp,
                        prevExp: "Expired within 90 days",
                      },
                    };
                  });
                }}
                checked={
                  order?.prevInsurerAndPolicyExp?.prevExp ===
                  "Expired within 90 days"
                    ? true
                    : false
                }
              />
              Expired within 90 days
            </label>
            <label>
              <input
                type="radio"
                name="expiryStatus"
                onChange={() => {
                  setOrder((prev) => {
                    return {
                      ...prev,
                      prevInsurerAndPolicyExp: {
                        ...order.prevInsurerAndPolicyExp,
                        prevExp: "Expired more than 90 days",
                      },
                    };
                  });
                }}
                checked={
                  order?.prevInsurerAndPolicyExp?.prevExp ===
                  "Expired more than 90 days"
                    ? true
                    : false
                }
              />
              Expired more than 90 days
            </label>
          </div>
        </div>
        <div className="inputs-box">
          <h4>Previous insurer & policy expiry</h4>
          <div className="inputs">
            <input type="text" placeholder="Search Company" />
          </div>
        </div>
        <div className="inputs-box">
          <h4>Previous Policy Type</h4>
          <div className="inputs">
            <label>
              <input
                type="radio"
                name="policyType"
                checked={
                  order?.prevInsurerAndPolicyExp?.prevPolicyType ===
                  "Comprehensive"
                    ? true
                    : false
                }
                onChange={() => {
                  setOrder((prev) => {
                    return {
                      ...prev,
                      prevInsurerAndPolicyExp: {
                        ...order.prevInsurerAndPolicyExp,
                        prevPolicyType: "Comprehensive",
                      },
                    };
                  });
                }}
              />
              Comprehensive
            </label>
            <label>
              <input
                type="radio"
                name="policyType"
                onChange={() => {
                  setOrder((prev) => {
                    return {
                      ...prev,
                      prevInsurerAndPolicyExp: {
                        ...order.prevInsurerAndPolicyExp,
                        prevPolicyType: "Third Party",
                      },
                    };
                  });
                }}
                checked={
                  order?.prevInsurerAndPolicyExp?.prevPolicyType ===
                  "Third Party"
                    ? true
                    : false
                }
              />
              Third Party
            </label>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Step6;
