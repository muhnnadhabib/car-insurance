import React, { useState } from "react";
import imgMARUTI from "../../assest/manufacturers/MARUTI.png";
import imgKIA from "../../assest/manufacturers/KIA.png";
import imgHYUNDAI from "../../assest/manufacturers/HYUNDAI.png";
import imgAUDI from "../../assest/manufacturers/AUDI.png";
import imgTATA from "../../assest/manufacturers/TATA.png";
import imgMERCEDESBENZ from "../../assest/manufacturers/MERCEDES-BENZ.png";
import imgMAHINDRAMAHINDRA from "../../assest/manufacturers/MAHINDRA-MAHINDRA.png";
import imgVOLKSWAGEN from "../../assest/manufacturers/VOLKSWAGEN.png";
import imgTOYOTA from "../../assest/manufacturers/TOYOTA.png";
import imgHONDA from "../../assest/manufacturers/HONDA.png";
// Start component
const Step2 = ({ order, setOrder, setShouldContinue }) => {
  const manufacturerList = [
    { name: "MARUTI", logo: imgMARUTI },
    { name: "HYUNDAI", logo: imgHYUNDAI },
    { name: "TATA", logo: imgTATA },
    { name: "TOYOTA", logo: imgTOYOTA },
    { name: "KIA", logo: imgKIA },
    { name: "HONDA", logo: imgHONDA },
    { name: "AUDI", logo: imgAUDI },
    { name: "MERCEDES-BENZ", logo: imgMERCEDESBENZ },
    { name: "VOLKSWAGEN", logo: imgVOLKSWAGEN },
    { name: "MAHINDRA AND MAHINDRA", logo: imgMAHINDRAMAHINDRA },
  ];

  const [searchWord, setSearchWord] = useState("");

  function selectManufacturer(manufacturer) {
    setOrder((prev) => {
      return { ...prev, manufacturer };
    });

    setShouldContinue(true);
  }
  function inputHandler(e) {
    const text = e.target.value;

    setSearchWord(text);
  }
  return (
    <div className="step-component step2">
      <p className="counter">STEP 2/6</p>
      <h2 className="step-title">Vehicle Manufacturer</h2>
      <input
        onChange={inputHandler}
        type="text"
        placeholder="Search your manufacturer"
      />

      <div className="manufacturer-list">
        {manufacturerList.map((manufacturer) => {
          if (
            searchWord !== "" &&
            !manufacturer.name.toLowerCase().includes(searchWord.toLowerCase())
          )
            return;
          return (
            <div
              className={
                order.manufacturer === manufacturer.name ? "selected-city" : ""
              }
              onClick={() => selectManufacturer(manufacturer.name)}
            >
              <img src={manufacturer.logo} alt={manufacturer.name} />
              <p>{manufacturer.name}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Step2;
