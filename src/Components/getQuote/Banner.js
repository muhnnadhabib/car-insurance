import React from "react";
import woman from "../../assest/woman.svg";
import guideImg from "../../assest/guide.png";

const Footer = () => {
  return (
    <div className="banner">
      <div className="guide-box">
        <div className="guide-text">
          <h4 className="guide-title">
            You have the opportunity to purchase anywhere your choose!
          </h4>
          <ul>
            <li>
              Dealers offering car insurance have no authority to prevent you
              from purchasing a policy from another insurer or broker (like us)
            </li>
            <li>
              Dealers are therefore unable to differentiate between services
              such as cashless/warranty based on the kind of car insurance you
              buy
            </li>
          </ul>
        </div>
        <img src={guideImg} alt="guide image" />
      </div>
    </div>
  );
};

export default Footer;
