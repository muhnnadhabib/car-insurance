import React from "react";

const CoverCheckBox = ({ text }) => {
  return (
    <label>
      <input type="checkbox" />
      {text}
    </label>
  );
};

export default CoverCheckBox;
