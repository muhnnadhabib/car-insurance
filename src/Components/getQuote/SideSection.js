import React from "react";

//import components

//import imgs
import logo from "../../assest/blue-logo.png";
import woman from "../../assest/woman.svg";
import editIcon from "../../assest/edit-icon.svg";

const SideSection = ({ order }) => {
  return (
    <div style={{ backgroundColor: "white" }} className="side-section">
      <div className="side-intro">
        <img src={logo} alt="logo" />
      </div>
      <div className="side-content">
        <div className="car-info-box">
          <img className="edit-btn" src={editIcon} />
          <h4>{`${order.manufacturer} ${order.model} ${order.variant}`}</h4>
          <p>Private car</p>
          <p>{order.year}</p>
          <p>{order.city}</p>
        </div>

        <div className="car-info-box">
          <img className="edit-btn" src={editIcon} />

          <p>
            <span className="blue">Previous Insurer: </span>Bajaj Allianz
          </p>
          <p>
            <span className="blue">Previous Policy Type: </span>
            {order.prevInsurerAndPolicyExp.prevPolicyType}
          </p>
          <p>
            <span className="blue">Policy Expiry Date: </span>04-May-2021
          </p>
        </div>
        <div className="car-info-box">
          <img className="edit-btn" src={editIcon} />
          <p>
            <span className="bold">Manufacture Date: </span>01-Sep-2019
          </p>
          <p>
            <span className="bold">Registration Date: </span>26-Sep-2019
          </p>
        </div>
      </div>
      <div className="side-footer">
        <div
          style={{ width: " 60%", margin: "0 auto" }}
          className="support-box"
        >
          <img src={woman} alt="a woman" />
          <button>Support</button>
        </div>
      </div>
    </div>
  );
};

export default SideSection;
