import React from "react";

const Card = ({ price, logo, addons }) => {
  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  const totalWithAddons = numberWithCommas(
    parseFloat(price) + parseFloat(addons)
  );

  const total = numberWithCommas(parseFloat(price));
  return (
    <div className="card">
      <img className="card-logo" src={logo} />
      <p className="card-price">
        <span>IDV</span>₹ {price}
      </p>
      <div className="card-addons">
        <p>Addons</p>
        <label>
          <input type="checkbox" />
          Zero Dep: +₹{addons}
        </label>
      </div>
      <button className="buy-btn">
        ₹{total} <i className="fas fa-arrow-right"></i>
      </button>
    </div>
  );
};

export default Card;
