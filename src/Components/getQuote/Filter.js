import React, { useState } from "react";
import Selectbox from "./Select";
import CoverCheckBox from "./CoverCheckBox";

const Filter = () => {
  const [popUpShow, setPopUpShow] = useState(false);
  return (
    <div className="filters-container">
      <Selectbox
        placeholder="Insured value(IDV)"
        choices={["test", "test", "test"]}
      />
      <Selectbox placeholder="PA Cover" choices={["test", "test", "test"]} />

      <label className="zero-dep">
        <input type="checkbox" />
        Zero Dep
      </label>

      <button
        onClick={() => {
          setPopUpShow((prev) => !prev);
        }}
      >
        <i className="fas fa-plus"></i>
        More Cover
      </button>
      <div className={`covers-pop ${popUpShow ? "pop-seen" : ""}`}>
        <span
          onClick={() => {
            setPopUpShow(false);
          }}
          className="close-pop"
        >
          <i className="fas fa-times"> </i>
        </span>
        <CoverCheckBox text="Drive Cover" />
        <CoverCheckBox text="Road Side Assistance" />
        <CoverCheckBox text="Road Side Assistance" />
        <CoverCheckBox text="Road Side Assistance" />
        <CoverCheckBox text="Road Side Assistance" />
        <CoverCheckBox text="Road Side Assistance" />
        <CoverCheckBox text="Road Side Assistance" />
        <CoverCheckBox text="Road Side Assistance" />
        <CoverCheckBox text="Road Side Assistance" />
        <div className="covers-footer">
          <span className="blue">7 Applied</span>
          <button>Update</button>
        </div>
      </div>
    </div>
  );
};

export default Filter;
