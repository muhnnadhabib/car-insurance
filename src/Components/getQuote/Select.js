import React from "react";

const Select = ({ placeholder, choices }) => {
  return (
    <label class="select" for="slct">
      <select id="slct" required="required">
        <option value="" disabled="disabled" selected="selected">
          {placeholder}
        </option>
        {choices.map((choice) => {
          return <option href="#">{choice}</option>;
        })}
      </select>
      <i className="fas fa-chevron-down"></i>
    </label>
  );
};

export default Select;
