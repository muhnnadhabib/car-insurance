import React, { useState } from "react";
import "./App.css";

//Importing The pages
import VehicleDetails from "./pages/VehicleDetails";
import GetQuote from "./pages/GetQuote";

//Importing The Components of Getting Quote

//React Route import
import { Route, BrowserRouter as Router } from "react-router-dom";

function App({}) {
  const [order, setOrder] = useState({});

  return (
    <Router>
      <Route path="/" exact>
        <VehicleDetails order={order} setOrder={setOrder} />
      </Route>
      <Route path="/get-quote" exact>
        <GetQuote order={order} setOrder={setOrder} />
      </Route>
    </Router>
  );
}

export default App;
