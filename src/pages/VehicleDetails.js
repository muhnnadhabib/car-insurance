import React, { useState } from "react";
//Importing The Components
import Header from "../Components/Header";
import SideSection from "../Components/SideSection";
import Content from "../Components/Content";
import Footer from "../Components/Footer";

const VehicleDetails = ({ order, setOrder }) => {
  const [currentStep, setCurrentStep] = useState(1);
  const [shouldContinue, setShouldContinue] = useState(false);
  return (
    <div className="App vehicleDetails">
      <Header />
      <SideSection
        currentStep={currentStep}
        setCurrentStep={setCurrentStep}
        order={order}
      />
      <Content
        shouldContinue={shouldContinue}
        setShouldContinue={setShouldContinue}
        order={order}
        setOrder={setOrder}
        currentStep={currentStep}
        setCurrentStep={setCurrentStep}
      />
      <Footer />
    </div>
  );
};

export default VehicleDetails;
