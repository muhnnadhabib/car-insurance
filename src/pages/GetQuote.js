import React, { useState } from "react";

//Importing The Components
import Header from "../Components/Header";
import SideGetQuote from "../Components/getQuote/SideSection";
import ContentGetQuote from "../Components/getQuote/Content";
import FilterGetQuote from "../Components/getQuote/Filter";
import BannerGetQuote from "../Components/getQuote/Banner";
const GetQuote = ({ order }) => {
  return (
    <div className="App getQuote">
      <Header />
      <SideGetQuote order={order} />
      <FilterGetQuote />
      <ContentGetQuote />
      <BannerGetQuote />{" "}
    </div>
  );
};

export default GetQuote;
